<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

// Footer Logos
$footerlogos = new FieldsBuilder('footer_logos');
	
	//Logos Repeater
	$footerlogos
	    ->addRepeater('logos', [
	      'min' => 1,
	      'max' => 4,
	      'button_label' => 'Add Logo',
	      'layout' => 'block',
	    ])
		    ->addImage('logo', [
				'label' => 'Logo',
				'ui' => $config->ui
			])
	    ->endRepeater();

// Footer Socials Fields
$footersocials = new FieldsBuilder('footer_socials');

	// Checkbox for icons/Links
	$footersocials
		->addCheckbox('socials', [
				'layout' => 'horizontal',
				'toggle' => 1,
		])
			->addChoices(
				['facebook' => 'Facebook'],
				['instagram' => 'Instagram'],
				['google' => 'Google Plus'],
				['pinterest' => 'Pinterest'],
				['twitter' => 'Twitter'],
				['youtube' => 'YouTube'],
				['linkedin' => 'LinkedIn']
			)

	// Url Fields
		->addText('facebook_url', [
			'label' => 'Facebook URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'facebook')
		
		->addText('instagram_url', [
			'label' => 'Instagram URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'instagram')

		->addText('google_url', [
			'label' => 'Google Plus URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'google')

		->addText('pinterest_url', [
			'label' => 'Pinterest URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'pinterest')

		->addText('twitter_url', [
			'label' => 'Twitter URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'twitter')

		->addText('youtube_url', [
			'label' => 'YouTube URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'youtube')

		->addText('linkedin_url', [
			'label' => 'LinkedIn URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'linkedin');

// WYSIWYG Tab
$footerwyiswyg = new FieldsBuilder('footerwyiswyg');
	
	$footerwyiswyg
	    ->addWysiwyg('footer_wysiwyg', [
	        'label' => 'Footer Content Area',
	        'instructions' => 'Will appear below the social icons and right above the copyright information',
	        'required' => 0,
	        'tabs' => 'text',
	        'toolbar' => 'full',
	        'media_upload' => 1,
	        'delay' => 0,
	    ]);
	

// Footer Options Tabs & Fields
$footeroptions = new FieldsBuilder('footer_options');

	$footeroptions
		->setLocation('options_page', '==', 'theme-footer-settings')
		// Footer Logos Tab
		->addTab('Footer Logos', ['placement' => 'left'])
			->addFields($footerlogos)
		// Social Icons Tab
		->addTab('Social Media', ['placement' => 'left'])
			->addFields($footersocials)
		// Footer WYSIWYG Tab
		->addTab('Footer Wysiwyg', ['placement' => 'left'])
			->addFields($footerwyiswyg);

		return $footeroptions;


	
