<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$card = new FieldsBuilder('card');

$card
	->addGroup('card', [
			'label' => 'Card Content'
		])

		//PreHeader		
		->addTrueFalse('pre_header_check', [
			'wrapper' => ['width' => 15],
			'label' => 'Add Pre-Header?'
			])
			->setInstructions('Add Optional Pre-header')
		->addText('pre_header', [
			'label' => 'Pre-header',
			'wrapper' => ['width' => 85]
		])
			->setInstructions('Small text that appears above header')
		->conditional('pre_header_check', '==', 1)

		// Header
		->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])
		->setInstructions('This is optional')

		// WYSIWYG
		->addWysiwyg('paragraph', [
			'label' => 'WYSIWYG',
			'ui' => $config->ui
		])
		->setInstructions('This is optional')
		
		//Button
		->addFields(get_field_partial('modules.button'))
		  
  ->endGroup();

return $card;