<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$deck = new FieldsBuilder('deck');

$deck
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addFields(get_field_partial('partials.grid_options'));

$deck
	->addTab('deck_type', ['placement' => 'left'])
		// Type of Deck
		->addSelect('type_select', [
			'label' => 'Select Deck Type',
			'ui' => $config->ui,
			'wrapper' => ['width' => 35]
		])
		->addChoices('Card', 'Icon', 'Article')

	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Deck Header'
	    	])
	    	->setInstructions('This is optional')

	    ->addTrueFalse('module_button', [
			'wrapper' => ['width' => 30]
			])
			->setInstructions('Check to add a CTA button for the module')
		->addLink('button', [
			'wrapper' => ['width' => 70]
		])
		->conditional('module_button', '==', 1)

	    // Post Relationship Field
		->addRelationship('article', [
		    'label' => 'Article Picker',
		    'conditional_logic' => [
				[
					[
					'field' => 'type_select',
					'operator' => '==',
					'value' => 'Article',
					],
				],
			],
	        'post_type' => 'post',
	        'min' => 1,
			'max' => 10,
		    'ui' => $config->ui,
		    'wrapper' => ['width' => 80]
	    ])

		//Repeater
		->addRepeater('deck', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Card',
		  'conditional_logic' => [
				[
					[
					'field' => 'type_select',
					'operator' => '!=',
					'value' => 'Article',
					],
				],
			],
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])
		->addTrueFalse('link_wrapper', [
			'wrapper' => ['width' => 30]
			])
			->setInstructions('Check to make whole card a clickable link')
		->addLink('link_url', [
			'wrapper' => ['width' => 70]
		])
		->conditional('link_wrapper', '==', 1)

		//Image 
		->addGroup('card_image', [
			'label' => 'Card Image'
		])
			->addTrueFalse('add_image', [
				'label' => 'Add Image to Card',
				'wrapper' => ['width' => 15]
				])
				->addImage('image', [
					'wrapper' => ['width' => 50]
				])
				->conditional('add_image', '==', 1)
		->endGroup()
		// Card
	  	->addFields(get_field_partial('modules.card'));
    
return $deck;