<?php 
/**
 * Plugin Name: Testimonial CPTS Plugin
 * Description: This is the Custom Post Type for FRN Testimonial.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_testimonial_cpts() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'sl_testimonial_cpts' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'sl_testimonial_cpts' ),
		'menu_name'             => __( 'Testimonials', 'sl_testimonial_cpts' ),
		'name_admin_bar'        => __( 'Testimonials', 'sl_testimonial_cpts' ),
		'archives'              => __( 'Testimonial Archives', 'sl_testimonial_cpts' ),
		'attributes'            => __( 'Testimonial Attributes', 'sl_testimonial_cpts' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'sl_testimonial_cpts' ),
		'all_items'             => __( 'All Testimonials', 'sl_testimonial_cpts' ),
		'add_new_item'          => __( 'Add New Testimonial', 'sl_testimonial_cpts' ),
		'add_new'               => __( 'Add New', 'sl_testimonial_cpts' ),
		'new_item'              => __( 'New Testimonial', 'sl_testimonial_cpts' ),
		'edit_item'             => __( 'Edit Testimonial', 'sl_testimonial_cpts' ),
		'update_item'           => __( 'Update Testimonial', 'sl_testimonial_cpts' ),
		'view_item'             => __( 'View Testimonial', 'sl_testimonial_cpts' ),
		'view_items'            => __( 'View Testimonials', 'sl_testimonial_cpts' ),
		'search_items'          => __( 'Search Testimonials', 'sl_testimonial_cpts' ),
		'not_found'             => __( 'Not found', 'sl_testimonial_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_testimonial_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_testimonial_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_testimonial_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_testimonial_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_testimonial_cpts' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'sl_testimonial_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'sl_testimonial_cpts' ),
		'items_list'            => __( 'Testimonial list', 'sl_testimonial_cpts' ),
		'items_list_navigation' => __( 'Testimonial list navigation', 'sl_testimonial_cpts' ),
		'filter_items_list'     => __( 'Filter Testimonial list', 'sl_testimonial_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'sl_testimonial_cpts' ),
		'description'           => __( 'Custom Post Type for FRN testimonials', 'sl_testimonial_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'testimonials','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_testimonial_cpts', $args );

}
add_action( 'init', 'sl_testimonial_cpts', 0 );
?>